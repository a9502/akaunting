<?php

namespace App\BulkActions\Auth;

use App\Abstracts\BulkAction;
use App\Models\Auth\Role;

class Roles extends BulkAction
{
    public $model = Role::class;

    public $actions = [
        'delete' => [
            'name' => 'general.deletex',
            'message' => 'bulk_actions.message.deletex',
            'permission' => 'delete-auth-roles',
        ],
    ];
}
